package com.datastructure.tree.bst;

public class BinarySearchTree {
	protected Node root;

	protected BinarySearchTree() {
		root = null;
	}

	public void insert(int value) {
		root = insertNode(root, value);
	}

	public void inordertraverse() {
		traverse(root);
	}
	
	public int height() {
		return maxDepth(root);
	}

	public void search(int value) {
		Node node = searchNode(root, value);
		if (node != null)
			System.out.println(node.data);
		else
			System.out.println("No such Node exist");

	}

	public void traverse(Node root) {
		if (null != root) {
			// left - root - right
			traverse(root.left);
			System.out.println(root.data);
			traverse(root.right);
		}
	}
	

	private Node insertNode(Node root, int value) {

		// checking root node is not empty
		if (null == root) {
			Node temp = new Node(value);
			root = temp;
			return root;
		}
		// check if value is less than current root node value then assign it to left
		// Node
		// this is recursive process.
		if (value < root.data)
			root.left = insertNode(root.left, value);

		// check if value is less than current root node value then assign it to right
		// Node
		// this is recursive process.
		if (value > root.data)
			root.right = insertNode(root.right, value);

		// return unchanged root node pointer.
		return root;
	}

	private Node searchNode(Node root, int value) {

		if (null == root || value == root.data)
			return root;

		if (root.data > value)
			return searchNode(root.left, value);
		else
			return searchNode(root.right, value);
	}
	
	private int maxDepth(Node root) {
		if (root == null) {
			return 0;
		}
		int lDepth = maxDepth(root.left);
		int rDepth = maxDepth(root.right);
		
		// add root node to height
		if (lDepth > rDepth)
			return lDepth+1;
		else
			return rDepth+1;
	}
	
	

/*	public static void main(String[] args) {

		BinarySearchTree tree = new BinarySearchTree();

		tree.insert(50);
		tree.insert(30);
		tree.insert(25);
		tree.insert(20);
		tree.insert(40);
		tree.insert(70);
		tree.insert(60);
		tree.insert(80);
		tree.insert(10);

		tree.inordertraverse();

		tree.search(20);
		tree.search(100);
	}*/
}
