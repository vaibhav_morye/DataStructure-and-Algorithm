package com.datastructure.tree.balancedbst;

import java.util.ArrayList;

import com.datastructure.tree.bst.BinarySearchTree;
import com.datastructure.tree.bst.Node;import ch.qos.logback.core.net.SyslogOutputStream;

// creating binary tree dose not guaranty that search will be fast
//or node will be stored in descending order.
// for this will will try to balance tree by traversing it in in-order and
//store it in array and creating binary tree from that. 

public class BalancedBST extends BinarySearchTree {


	BalancedBST() {
		super();
	}

	public void storeBSTNode(Node root, ArrayList<Node> nodes) {

		if (root == null) {
			return;
		}

		storeBSTNode(root.left, nodes);
		nodes.add(root);
		storeBSTNode(root.right, nodes);

	}

	public Node buildTreeUtil(ArrayList<Node> nodes, int start, int end) {
		//System.out.println("start "+start+" end "+end);
		if (start > end) {
			return null;
		}
		int mid = (start + end) / 2;
		//System.out.println("start "+start+" end "+end+ " mid "+mid);
		Node node = nodes.get(mid);
		
		node.left = buildTreeUtil(nodes, start, mid - 1);
		node.right = buildTreeUtil(nodes, mid + 1, end);

		return node;

	}

	Node buildTree(Node root) {
		// Store nodes of given BST in sorted order
		ArrayList<Node> nodes = new ArrayList<Node>();
		storeBSTNode(root, nodes);
		for(Node node:nodes)
			System.out.print(node.data+", ");

		// Constructs BST from nodes[]
		int n = nodes.size();
		return buildTreeUtil(nodes, 0, n - 1);
	}

	void preOrder(Node node) {

		if (node == null)
			return;

		System.out.print(node.data + " ");
		preOrder(node.left);
		preOrder(node.right);
	}

	public static void main(String[] args) {
		
		BalancedBST tree = new BalancedBST();
		tree.root = new Node(10);
		tree.root.left = new Node(8);
		tree.root.left.left = new Node(7);
		tree.root.left.left.left = new Node(6);
		tree.root.left.left.left.left = new Node(5);

		tree.root = tree.buildTree(tree.root);
		System.out.println("Preorder traversal of balanced BST is :");
		tree.preOrder(tree.root);
	}
}
