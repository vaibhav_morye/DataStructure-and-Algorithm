
package com.datastructure.linklist.doblylinkedlist;

class Node {
	Node prev;
	Node next;
	int val;

	Node(int val) {
		this.val = val;
		prev = next = null;
	}
}

public class DoublyLinkedList {

	private Node head = null;
	private Node tail = null;

	private void addToBack(int value) {

		Node tempNode = new Node(value);

		// If head is null then add node as first Node assign tail and head same as it is first Node.
		if (head == null) {
			head = tempNode;
			tail = tempNode;
		} else {
			//add node at last give last node reference to previous Node as next.
			tail.next = tempNode;
			// as it's doubly linked list give previous Node reference to current node
			tempNode.prev = tail;
			// set last node as tail 
			tail = tempNode;
		}
	}

	private void printforwordandReverse() {
		for (Node curr = this.head; curr != null; curr = curr.next)
			System.out.println(curr.val);

		System.out.println();

		for (Node curr = this.tail; curr != null; curr = curr.prev)
			System.out.println(curr.val);
	}

	public static void main(String args[]) {

		// Create a List object
		DoublyLinkedList list = new DoublyLinkedList();

		// Add a few values
		list.addToBack(10);
		list.addToBack(20);
		list.addToBack(30);

		list.printforwordandReverse();
	}

}
